/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _MSGRX_PUB_H    /* Guard against multiple inclusion */
#define _MSGRX_PUB_H


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif


    void app1PostToRxQ_ISR(char b,BaseType_t *pxHigherPriorityTaskWoken);
    void app1PostToRxQ(char b);

    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
