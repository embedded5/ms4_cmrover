
#include "motor.h"

void motorInit() {
    DRV_TMR1_Start(); // Encoder Timer
    DRV_TMR2_Start(); // Encoder Timer
    
    // Enable Encoder Timers
    // <-- TODO: Disable interrupts and move to interrupt based read -->
    //PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_3);
    //PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_4);
    
  
 //   PLIB_PORTS_PinModeSelect    analog? digital?
    PLIB_PORTS_DirectionOutputSet (PORTS_ID_0, PORT_CHANNEL_B, 0xFF);
    PLIB_PORTS_Clear (PORTS_ID_0, PORT_CHANNEL_B, 0x00);
    
    // Output Compare Enables and Starts
    DRV_OC0_Enable();
    DRV_OC1_Enable();
    DRV_OC0_Start();
    DRV_OC1_Start();
}

void rightMotorWrite(int val) {
    DRV_0C0_Write(val);
}

void leftMotorWrite(int val) {
    DRV_0C1_Write(val);
}

int rightEncoderRead() {
    return DRV_TMR1_CounterValueGet();
}

int leftEncoderRead() {
    return DRV_TMR2_CounterValueGet();
}
