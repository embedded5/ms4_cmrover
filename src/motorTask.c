#include "motortask.h"
#include "motorTask_pub.h"
#include "motor.h"
#include "messageID.h"
#include "msgTx_pub.h"

void postMotorStats();

// msg buffer setup
#define MSG_BUFFER_HEIGHT 10
#define MSG_BUFFER_WIDTH 2
unsigned int motorBuffer[MSG_BUFFER_HEIGHT][MSG_BUFFER_WIDTH];
const unsigned int read_encoders[] = {CMD_READ_ENCODERS,0};
// end buffer setup

// MsgQ allocation
QueueHandle_t xQueueMotor;

// motor control values
uint32_t encoder_r = 0;
uint32_t encoder_l = 0;
int debug_counter = 0;
int motor_r = 0;
int motor_l = 0;
////////////
uint32_t currentMilliseconds_l = 0;
uint32_t currentMilliseconds_r = 0;
uint32_t timelength_l = 0;
uint32_t timelength_r = 0;
uint32_t motorStart_l = 0;
uint32_t motorStart_r = 0;
//////PID/////
//double pidTerm = 0;
//double error = 0;
//double area_error = 0;
//double last_error = 0;
//double D_error = 0;
//double Kp = 0.45, Ki = 0.015, Kd = 1;
//double dt;
//////////////

void MOTORTASK_Initialize ( void )
{
    int i=0;
    while (i<MSG_BUFFER_HEIGHT) {
        motorBuffer[i][0] = MSG_EMPTY;
        motorBuffer[i][1] = 0;
        i += 1;
    }
    xQueueMotor = xQueueCreate(10, sizeof( unsigned int* ));
    PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_5);
}

void MOTORTASK_Tasks ( void )
{   
    motorInit();    
    // This timer will be started by Rx task for now, to prevent sending stats before wifly is ready
    //DRV_TMR0_Start(); // PWM Timer and millisecond timer
    PLIB_INT_SourceEnable(INT_ID_0,INT_SOURCE_TIMER_2); // enable millisecond counting interrupts
    
    DRV_TMR3_Start(); // sensor read timer

    int *b = motorBuffer[0];
   
    while (1) {       
        xQueueReceive(xQueueMotor,&b,portMAX_DELAY);
        switch (b[0]) {
            case CMD_READ_ENCODERS:
                encoder_r = rightEncoderRead();
                encoder_l = leftEncoderRead();                            
                b[0] = MSG_EMPTY;
                break;
                
            case CMD_PWM_RIGHT:               
                motor_r = b[1];
                rightMotorWrite(motor_r);
                motorStart_r = currentMilliseconds_r;               
                b[0] = MSG_EMPTY;               
                break;
                
            case CMD_PWM_LEFT:
                motor_l = b[1];
                leftMotorWrite(motor_l);
                motorStart_l = currentMilliseconds_l;
                b[0] = MSG_EMPTY;
                break;
                
            case CMD_GET_MOTOR_STATS:
                b[0] = MSG_EMPTY;
                postMotorStats();
                break; 
                
            case CMD_STOP:
                leftMotorWrite(100);
                rightMotorWrite(100);
                b[0] = MSG_EMPTY;
                break;
                
            case CMD_DISTANCE: 
                if((b[1]==0)){
                    b[0]= MSG_EMPTY;
                    break;
                }               
                else{
                timelength_l=170*b[1];
                timelength_r=170*b[1];
                b[0]=MSG_EMPTY;
                break; 
                }
                           
             //0 go straight; 1 left; 2 right; 3 back; 
            case CMD_DIRECTION:
                if(b[1]==0){ 
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 0);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 0);
                }
                else if(b[1]==1){ 
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 1);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 0);
                }
                else if(b[1]==2){ 
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 0);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 1);
                }
                else if(b[1]==3){ 
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 1);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 1);
                }
                else {
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_0, 0);
                    PLIB_PORTS_PinWrite (PORTS_ID_0, PORT_CHANNEL_B, PORTS_BIT_POS_1, 0);
                }
                b[0]=MSG_EMPTY;
                break;
                   
            case MSG_TIME_MS: 
               currentMilliseconds_l += b[1];
               currentMilliseconds_r += b[1];
               if(currentMilliseconds_l-motorStart_l>timelength_l){
                    leftMotorWrite(0);                 
                }           
               
               if(currentMilliseconds_r-motorStart_r>timelength_r){
                    rightMotorWrite(0);                 
                }  
                         
                b[0]= MSG_EMPTY;
                break;         
        }
    }
   
}

void postToMotorQ(int *b)
{
    xQueueSend(xQueueMotor, &b, portMAX_DELAY);
}
/* Function that sends current motorStats values to the Tx Value Q */
void postMotorStats()
{
    int i=0;
    bool done = false;
    // find empty slots
    int empty[5] = {-1,-1,-1,-1,-1}; //array of empty indexes in the buffer
    int e = 0;
    while (i < MSG_BUFFER_HEIGHT && e < 5) {
        //postToMsgQ()
        if (motorBuffer[i][0] == MSG_EMPTY) {
            empty[e] = i;
            e += 1;
        }
        i += 1;
    }
    
    if (e == 5) {
        motorBuffer[empty[0]][0] = MSG_SPEED_RIGHT;
        motorBuffer[empty[0]][1] = motor_r;
        postToMsgQ(motorBuffer[empty[0]]);
        
        motorBuffer[empty[1]][0] = MSG_SPEED_LEFT;
        motorBuffer[empty[1]][1] = motor_l;
        postToMsgQ(motorBuffer[empty[1]]);
        
        motorBuffer[empty[2]][0] = MSG_ENCODER_RIGHT;
        motorBuffer[empty[2]][1] = encoder_r;
        postToMsgQ(motorBuffer[empty[2]]);
        
        motorBuffer[empty[3]][0] = MSG_ENCODER_LEFT;
        motorBuffer[empty[3]][1] = encoder_l;
        postToMsgQ(motorBuffer[empty[3]]);
        
        motorBuffer[empty[4]][0] = CMD_SEND_MOTOR_STATS;
        motorBuffer[empty[4]][1] = 1;
        postToMsgQ(motorBuffer[empty[4]]);
        
    } else {
        app1PostToTxQ('e');
    }
    
}
void postToMotorQ_ISR(int ID, int VAL,BaseType_t *pxHigherPriorityTaskWoken) 
{
    int i = 0;
    while (i < MSG_BUFFER_HEIGHT) {
        //postToMsgQ()
        if (motorBuffer[i][0] == MSG_EMPTY) {
            break;
        }
        i += 1;
    }
    if (i >= MSG_BUFFER_HEIGHT) {
        i = 0;
    }
    motorBuffer[i][0] = ID;
    motorBuffer[i][1] = VAL;
    int *p = &motorBuffer[i];
    xQueueSendFromISR( xQueueMotor, &p, pxHigherPriorityTaskWoken );
}


//                       register value   target speed,     speed right now
//double updatePID_l(double command, double targetValue, double currentValue)
//{
// dt = double((time-motorStart_l) / 1000);
// error = (targetValue) - (currentValue);
// area_error = (error * dt) + area_error;
// D_error = (Kd * (error - last_error));
// pidTerm = (Kp * error) + (Ki * area_error) + D_error;
// last_error = error;
// return command + double(pidTerm);
//}
//
//double updatePID_r(double command, double targetValue, double currentValue)
//{
// dt = double((time-motorStart_r) / 1000);
// error = (targetValue) - (currentValue);
// area_error = (error * dt) + area_error;
// D_error = (Kd * (error - last_error));
// pidTerm = (Kp * error) + (Ki * area_error) + D_error;
// last_error = error;
// return command + double(pidTerm);
//}
/*******************************************************************************
 End of File
 */
