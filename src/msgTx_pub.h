/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _MSGTX_PUB_H    /* Guard against multiple inclusion */
#define _MSGTX_PUB_H


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

    
    void app1GetFromTxQ(BaseType_t *pxHigherPriorityTaskWoken);
    void app1PostToTxQ(char b);
    
    void postToMsgQ(int *b);
    void postToTxMsgQ_ISR(int ID, int val,BaseType_t *pxHigherPriorityTaskWoken);
    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
